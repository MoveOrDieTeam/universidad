<?php

use Faker\Generator as Faker;

$factory->define(\App\Apunte::class, function (Faker $faker){

    return[
        'materia_id' => \App\Materia::all()->random()->id,
        'usuario_id' => \App\User::all()->random()->id,
        'fecha' => $faker -> dateTimeThisYear(),
        'anio' => '2018',
        'cuatrimestre' => $faker -> boolean() ? 1: 2,
        'es_privado' => $faker -> boolean(),
        'titulo' => $faker -> word(),
        'descripcion' => $faker -> paragraph(1),
        'path' => '\home\apuntes'  . $faker -> name() . $faker -> fileExtension(),

    ];

});
