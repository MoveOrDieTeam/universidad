<?php
/**
 * Created by PhpStorm.
 * User: nicole
 * Date: 23/12/18
 * Time: 22:48
 */

use Faker\Generator as Faker;

$factory->define(\App\Materia::class, function (Faker $faker){

    return[
        'nombre'=> $faker ->word,
        'codigo'=> $faker->hexColor,
    ];

});
