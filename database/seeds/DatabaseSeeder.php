<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Instituto;
use App\User;
use App\Materia;
use App\Carrera;
use App\Apunte;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $cantCarreras=5;
        $cantInstitutos=3;
        $cantMaterias= 20;
        $cantUsuarios=10;
        $cantApuntes=200;

        $this->truncateTables([
            'apuntes',
            'materias',
            'carreras',
            'carrera_materia',
            'institutos',
            'users'
        ]);

        factory(User::class,$cantUsuarios)->create();
        factory(Instituto::class,$cantInstitutos)->create();
        factory(Carrera::class, $cantCarreras)->create();
        factory(Materia::class, $cantMaterias)->create()->each(
            function ($materia){
                $carreras = Carrera::all()->random(mt_rand(1,3))->pluck('id');
                $materia->carreras()->attach($carreras);
            }
        );
        factory(Apunte::class,$cantApuntes)->create();

    }


    public function truncateTables(array $tables)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        foreach ($tables as $table) {
            DB::table($table)->truncate();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    }

}
