<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableApuntes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apuntes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('materia_id');
            $table->integer('anio');
            $table->char('cuatrimestre');
            $table->date('fecha');
            $table->string('titulo');
            $table->string('descripcion')->nullable();
            $table->string('path');
            $table->unsignedInteger('usuario_id');
            $table->boolean('es_privado');
            $table->timestamps();
            $table->foreign('materia_id')->references('id')->on('materias');
            $table->foreign('usuario_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apuntes');
    }
}
