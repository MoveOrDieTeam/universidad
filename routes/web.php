<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/login', function () {
    return view('login');
});

Route::get('/mis-apuntes', function () {
    return view('misapuntes');
});

Route::get('/test1', function () {
    return view('template.main');
});

Route::get('/test', function () {
    return view('test');
});


Route::get('/perfil/{id}/subir-apunte',function(){
    return view('perfil.subirApunte');
});


Route::resource('usuarios', 'UsuariosController');

Route::resource('apuntes', 'ApuntesController');

Route::resource('perfil', 'PerfilController');

Auth::routes();

