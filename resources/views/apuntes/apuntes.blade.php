@extends('layouts.app')
@section('content')


    {{--Quizás se puede ahorrar codigo solamente cambiando el tipo de mensaje que se muestra pero se cuela un string feo, #CONSULTA--}}
    @if(Session::has('success_message_update'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('success_message_update')}}
        </div>
    @elseif(Session::has('success_message_store'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('success_message_store')}}
        </div>
    @elseif(Session::has('success_message_destroy'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('success_message_destroy')}}
        </div>
    @endif

    @if(Session::has('failure_message_destroy'))
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{Session::get('failure_message_destroy')}}
        </div>
    @endif

    <div class="container body">
        <div class="main_container">

            @yield('content')

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">

                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Lista de apuntes<small>Agrupado por materias</small></h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-book"></i></a>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="/apuntes/create">Crear un nuevo apunte</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <table class="table table-striped jambo_table ">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Título</th>
                                                <th>Fecha</th>
                                                <th>Es privado</th>
                                                <th>Materia</th>
                                                <th>Opciones</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($apuntes as $apunte)
                                                <tr>
                                                    <th scope="row">{{$apunte->id}}</th>
                                                    <td>{{$apunte->titulo}}</td>
                                                    <td>{{$apunte->fecha}}</td>
                                                    @php $aux = $apunte->es_privado @endphp
                                                    @if($aux == 0)
                                                        <td>No</td>
                                                    @else
                                                        <td>Sí</td>
                                                    @endif
                                                    <td>{{$apunte->materia_nombre}}</td>
                                                    <td><a class="btn btn-success btn-xs" href="/apuntes/{{$apunte->id}}"><i class="fa fa-book"></i> Ver</a></td>
                                                </tr>

                                            @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--@include('template.components.content')--}}
        </div>
    </div>

@endsection