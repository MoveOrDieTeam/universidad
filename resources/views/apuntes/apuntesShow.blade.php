@extends('layouts.app')
@section('content')

    <div class="container body">
        <div class="main_container">

            @yield('content')

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">

                            <div class="card-body">
                                <div class="card">
                                    <div class="card-header">
                                        <h2><b>{{$apunte->titulo}}</b></h2>
                                    </div>
                                    <div class="card-body">
                                        <h4 class="card-title"><b>Descripción: </b><i>{{$apunte->descripcion}}</i></h4>
                                        <h5 class="card-title"><b>Materia: </b>{{$materia->nombre}}.</h5>
                                        @php $auxCuatri = $apunte->cuatrimestre @endphp
                                        @if($auxCuatri == 1)
                                            <h5 class="card-text"><b>Fecha a la que pertenece: </b>1° cuatrimestre del año {{$apunte->anio}}.</h5>
                                        @else
                                            <h5 class="card-text"><b>Fecha a la que pertenece: </b>2° cuatrimestre del año {{$apunte->anio}}.</h5>
                                        @endif
                                        <h5 class="card-text"><b>Fecha de creación: </b>{{$apunte->fecha}}.</h5>
                                        <h5 class="card-text"><b>Usuario: </b>{{$usuario->name}}.</h5>
                                        @php $aux = $apunte->es_privado @endphp
                                        @if($aux == 0)
                                            <h5 class="card-text"><b>Es privado:</b> No</h5>
                                        @else
                                            <h5 class="card-text"><b>Es privado:</b> Sí</h5>
                                        @endif
                                        <h5 class="card-text"><b>Path: </b>{{$apunte->path}}</h5>

                                        <form action="/apuntes/{{$apunte->id}}" method="POST">
                                            @method('DELETE')
                                            <a href="/apuntes/{{$apunte->id}}/edit" type="button" class="btn btn-secondary"><i class="fa fa-edit"></i> Editar</a>
                                            <button href="/apuntes/{{$apunte->id}}" type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</button>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection