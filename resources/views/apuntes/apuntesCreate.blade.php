@extends('layouts.app')
@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <div class="container body">
        <div class="main_container">

            @yield('content')

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <form class="form-horizontal form-label-left input_mask" action="/apuntes" method="POST">
                            {{ csrf_field() }}
                            <div class="card">
                                <div class="card-body">
                                    <div class="card">
                                        <div class="card-header">
                                            <input class="form-control" placeholder="Título" type="text" name="tit">

                                        </div>
                                        <div class="card-body">
                                            <input class="form-control" placeholder="Descripción" type="text" name="desc">

                                            <h5 class="card-title"><b>Seleccionar materia: </b></h5>
                                            <select class="form-control" name="mat">
                                                @foreach($listaMaterias as $materiaOpcion)
                                                    <option value="{{$materiaOpcion->id}}" >{{$materiaOpcion->nombre}}</option>
                                                @endforeach
                                            </select>

                                            <h5 class="card-text"><b>Fecha a la que pertenece: </b></h5>
                                                <label><input checked="" value="1" id="opcionCuatri1" name="radioCuatri" type="radio"> Primer cuatrimestre
                                                    <input value="2" id="opcionCuatri2" name="radioCuatri" type="radio"> Segundo cuatrimestre</label>

                                            <select class="form-control" name="anioElegido">
                                                @for($i = 2011; $i <= date("Y"); $i++)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>

                                            <h5 class="card-text"><b>Fecha de creación: </b> (se creará con la actual)</h5>
                                            <input disabled name="fechaActual" value="{{date("Y-d-m") }}">

                                            <h5 class="card-text"><b>Seleccionar usuario: </b></h5>
                                            <select class="form-control" name="usuarioElegido">
                                                @foreach($listaUsuarios as $usuarioOpcion)
                                                    <option value="{{$usuarioOpcion->id}}">{{$usuarioOpcion->name}}</option>
                                                @endforeach
                                            </select>

                                            <label><input checked value="1" id="opcionPrivadoSi" name="radioPrivado" type="radio"> Público</label>
                                            <label><input value="0" id="opcionPrivadoNo" name="radioPrivado" type="radio"> Privado</label>

                                            <h5 class="card-text"><b>Path: (se incluirá en el directorio actual) </b></h5>
                                            <input disabled="disabled" class="form-control" placeholder="\home\apuntes\" type="text">
                                            <br>

                                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Crear</button>
                                            <a href="/apuntes" type="button" class="btn btn-secondary"><i class="fa fa-times"></i> Cancelar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection