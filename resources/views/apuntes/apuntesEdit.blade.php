@extends('layouts.app')
@section('content')

    <div class="container body">
        <div class="main_container">

            @yield('content')

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <form class="form-horizontal form-label-left input_mask" action="/apuntes/{{$apunte->id}}" method="POST">
                            {{ method_field('PUT') }}
                            {{ csrf_field() }}
                            <div class="card">
                                <div class="card-body">
                                    <div class="card">
                                        <div class="card-header">
                                            <h2><b>{{$apunte->titulo}}</b></h2>
                                            <input class="form-control" placeholder="Título" type="text" name="tit">

                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title"><b>Descripción: </b><i>{{$apunte->descripcion}}</i></h4>
                                            <input class="form-control" placeholder="Descripción" type="text" name="desc">

                                            <h5 class="card-title"><b>Materia: </b>{{$materia->nombre}}.</h5>
                                            <select class="form-control" name="mat">
                                                @foreach($listaMaterias as $materiaOpcion)
                                                    @if($materiaOpcion->id == $apunte->materia_id)
                                                        <option value="{{$materiaOpcion->id}}" selected>{{$materiaOpcion->nombre}}</option>
                                                    @else
                                                        <option value="{{$materiaOpcion->id}}">{{$materiaOpcion->nombre}}</option>
                                                    @endif
                                                @endforeach
                                            </select>

                                            <h5 class="card-text"><b>Fecha a la que pertenece: </b></h5>
                                                @if($apunte->cuatrimestre == 1)
                                                    <h5>1° cuatrimestre del año {{$apunte->anio}}.</h5>
                                                    <label><input checked="" value="1" id="opcionCuatri1" name="radioCuatri" type="radio"> Primero
                                                    <input value="2" id="opcionCuatri2" name="radioCuatri" type="radio"> Segundo</label>
                                                @else
                                                    <h5>2° cuatrimestre del año {{$apunte->anio}}.</h5>
                                                    <label><input value="1" id="opcionCuatri1" name="radioCuatri" type="radio"> Primero
                                                    <input checked="" value="2" id="opcionCuatri2" name="radioCuatri" type="radio"> Segundo</label>
                                                @endif
                                            <select class="form-control" name="anioElegido">
                                            @for($i = 2011; $i <= date("Y"); $i++)
                                                @if($i == $apunte->anio)<option selected> {{$i}}</option>
                                                @else<option> {{$i}}</option>
                                                @endif
                                            @endfor
                                            </select>

                                            <h5 class="card-text"><b>Fecha de creación: </b>{{$apunte->fecha}}.</h5>

                                            <h5 class="card-text"><b>Usuario: </b>{{$usuario->name}}.</h5>
                                            @php $aux = $apunte->es_privado @endphp
                                            @if($aux == 0)
                                                <h5 class="card-text"><b>Es privado:</b> No</h5>
                                                <label><input value="1" id="opcionPrivadoSi" name="radioPrivado" type="radio"> Sí</label>
                                                <label><input checked="" value="0" id="opcionPrivadoNo" name="radioPrivado" type="radio"> No</label>
                                            @else
                                                <h5 class="card-text"><b>Es privado:</b> Sí</h5>
                                                <label><input checked="" value="1" id="opcionPrivadoSi" name="radioPrivado" type="radio"> Sí</label>
                                                <label><input value="0" id="opcionPrivadoNo" name="radioPrivado" type="radio"> No</label>
                                            @endif

                                            <h5 class="card-text"><b>Path: </b>{{$apunte->path}}</h5>

                                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Guardar</button>
                                            <a href="/apuntes/{{$apunte->id}}" type="button" class="btn btn-secondary"><i class="fa fa-times"></i> Cancelar</a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection