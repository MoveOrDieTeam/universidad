@extends('layouts.app')

@section('content')

<div class="container body">
    <div class="main_container">

        @yield('content')

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Perfil <small></small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <!--Informacion del Perfil-->
                            <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                                <div class="profile_img">
                                    <div id="crop-avatar">
                                        <!-- Current avatar -->
                                        <img class="img-responsive avatar-view" src="/assets/images/picture.jpg" alt="Avatar" title="Change the avatar">
                                    </div>
                                </div>
                                <h3>{{$usuario->name}}</h3>
                                <br>

                                <ul class="list-unstyled user_data">
                                    <li><i class="fa fa-graduation-cap user-profile-icon"></i> <strong>  Carrera: </strong> {{$carrera->nombre}}
                                    </li>

                                    <li>
                                        <i class="fa fa-file user-profile-icon"></i><strong>  Apuntes:</strong> {{$cantApuntes}}
                                    </li>
                                </ul>
                                <br>

                                <a class="btn btn-primary" href="/perfil/{{$usuario->id}}/edit"><i class="fa fa-edit m-right-xs"></i> Editar perfil</a>
                                <br>
                                <a class="btn btn-success" href="/perfil/{{$usuario->id}}/subir-apunte"><i class="fa fa-upload m-right-xs"></i> Subir apunte</a>
                                <br>
                            </div>

                            <!-- Fin de la informacion del Perfil-->


                            <!--Comienzo del TAB-->
                            <div class="col-md-9 col-sm-9 col-xs-12">

                                <div class="profile_title">
                                    <div class="col-md-6">
                                        <h2>{{$usuario->name}}</h2>
                                    </div>
                                </div>

                                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Actividad reciente</a>
                                        </li>
                                        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Apuntes</a>
                                        </li>
                                    </ul>
                                    <div id="myTabContent" class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

                                            <!-- Comienzo de Actividad reciente-->
                                            <ul class="messages">
                                                <li>
                                                    <img src="/assets/images/img.jpg" class="avatar" alt="Avatar">
                                                    <div class="message_date">
                                                        <h3 class="date text-info">24</h3>
                                                        <p class="month">May</p>
                                                    </div>
                                                    <div class="message_wrapper">
                                                        <h4 class="heading">Santiago Doti</h4>
                                                        <blockquote class="message">Nicole Denon es una capa estoy seguro. El domingo invito un asado.</blockquote>
                                                        <br>
                                                        <p class="url">
                                                            <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                                            <a href="#"><i class="fa fa-paperclip"></i> User Acceptance Test.doc </a>
                                                        </p>
                                                    </div>
                                                </li>
                                                <li>
                                                    <img src="/assets/images/img.jpg" class="avatar" alt="Avatar">
                                                    <div class="message_date">
                                                        <h3 class="date text-error">21</h3>
                                                        <p class="month">May</p>
                                                    </div>
                                                    <div class="message_wrapper">
                                                        <h4 class="heading">Matias Benary</h4>
                                                        <blockquote class="message">El domingo voy, llevo las birras, Nicole no debe pagar nada.</blockquote>
                                                        <br>
                                                        <p class="url">
                                                            <span class="fs1" aria-hidden="true" data-icon=""></span>
                                                            <a href="#" data-original-title="">Download</a>
                                                        </p>
                                                    </div>
                                                </li>
                                            </ul>
                                            <!-- Fin de la actividad reciente-->

                                        </div>


                                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                            <!-- Comienzo de apuntes-->

                                            <!--<a href="#set-3" class="hi-icon hi-icon-pencil">Edit</a>-->

                                            <section class="ib-container" id="ib-container">
                                            @foreach($apuntes as $apunte)
                                                <article class="">
                                                    <header>
                                                        <h3><a target="_blank" href="#">{{$apunte->titulo}}</a></h3>
                                                        <span>{{$apunte->fecha}}</span>
                                                    </header>
                                                    <p>{{$apunte->descripcion}}</p>
                                                </article>

                                            @endforeach
                                            </section>
                                            <!-- Fin de apuntes-->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection
@section('scripts')

    <!--Iconos-->
    <!--<link href="/css/icons/component.css" rel="stylesheet">
    <script src="/js/icons/component.js"></script>
    <script src="/js/icons/modernizr.custom.js"></script>-->

    <!--Documentos-->
    <link href="/css/apuntes/style.css" rel="stylesheet">
    <link href="/css/apuntes/demo.css" rel="stylesheet">

    <script type="text/javascript">
        $(function() {

            var $container	= $('#ib-container'),
                $articles	= $container.children('article'),
                timeout;

            $articles.on( 'mouseenter', function( event ) {

                var $article	= $(this);
                clearTimeout( timeout );
                timeout = setTimeout( function() {

                    if( $article.hasClass('active') ) return false;

                    $articles.not( $article.removeClass('blur').addClass('active') )
                        .removeClass('active')
                        .addClass('blur');

                }, 65 );

            });

            $container.on( 'mouseleave', function( event ) {

                clearTimeout( timeout );
                $articles.removeClass('active blur');

            });

        });
    </script>

@endsection