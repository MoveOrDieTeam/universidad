@extends('layouts.app')

@section('content')

<div class="container body">
    <div class="main_container">

        @yield('content')

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Inspeccionar usuario</div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                                <div class="col-md-12 col-sm-12 col-xs-12 profile_details">
                                    <div class="well profile_view">
                                        <div class="col-sm-12">
                                            <h4 class="brief"><i>{{$carrera->nombre}}</i></h4>
                                            <div class="left col-xs-7">
                                                <h2>{{$usuario->name}}</h2>
                                                <!--<p><strong>About: </strong> Web Designer / UX / Graphic Artist / Coffee Lover </p> -->
                                                <ul class="list-unstyled">
                                                    <li><i class="fa fa-at"></i> <strong>E-mail:</strong> {{$usuario->email}} </li>
                                                    <li><i class="fa fa-graduation-cap"></i> <strong>Carrera: </strong>{{$carrera->nombre}}</li>
                                                    <li><i class="fa fa-file"></i> <strong>Apuntes: </strong>{{$apuntes}}</li>
                                                </ul>
                                            </div>
                                            <div class="right col-xs-5 text-center">
                                                <img src="/assets/images/img.jpg" alt="Imagen de perfil" class="img-circle img-responsive">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 bottom text-center">

                                            <div class="col-xs-12 col-sm-12 emphasis">
                                                <form action="/usuarios/{{$usuario->id}}" method="POST">
                                                    @method('DELETE')
                                                    <a class="btn btn-success" href="/perfil/{{$usuario->id}}"><i class="fa fa-user m-right-xs"></i> Ver usuario</a>
                                                    <a class="btn btn-primary" href="/usuarios/{{$usuario->id}}/edit"><i class="fa fa-edit m-right-xs"></i> Editar usuario</a>
                                                    <button type="submit" class="btn btn-danger "><i class="fa fa-times"></i> Eliminar Usuario</button>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
