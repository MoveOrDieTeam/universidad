@extends('layouts.app')
@section('content')

<div class="container body">
    <div class="main_container">

        @yield('content')

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Dashboard</div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Usuarios <small></small></h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="/usuarios/create">Crear usuario</a>
                                                    </li>
                                                    <li><a href="#">Settings 2</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-striped jambo_table bulk_action">
                                            <thead>
                                            <tr class="headings">
                                                <th class="column-title"># </th>
                                                <th class="column-title">Nombre</th>
                                                <th class="column-title">Email</th>
                                                <th class="column-title">Es admin</th>
                                                <th class="column-title">Carrera</th>
                                                <th class="column-title">Opciones</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach($usuarios as $usuario)
                                                <tr role="row" class="odd pointer">
                                                    <td class=" ">{{$usuario->id}}</td>
                                                    <td class=" ">{{$usuario->name}}</td>
                                                    <td class=" ">{{$usuario->email}}</td>
                                                    <td class=" ">{{$usuario->is_admin}}</td>
                                                    <td class=" ">{{$usuario->carrera_nombre}}</td>
                                                    <td class=" last"><a class="btn btn-success btn-xs" href="/usuarios/{{$usuario->id}}"><i class="fa fa-user m-right-xs"></i> Ver</a></td>

                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--        @include('template.components.content')--}}
    </div>
</div>

@endsection
