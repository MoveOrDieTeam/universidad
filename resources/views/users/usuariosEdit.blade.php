@extends('layouts.app')
@section('content')

<div class="container body">
    <div class="main_container">

        @yield('content')

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Editar usuario</div>
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>{{$usuario->name}}<small>{{$usuario->email}}</small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Settings 1</a>
                                            </li>
                                            <li><a href="#">Settings 2</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content" style="display: block;">



                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <p>¡Oops! Ha ocurrido un error. Por favor corrija los siguientes campos:</p>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <br>
                                <form class="form-horizontal form-label-left input_mask" action="/usuarios/{{$usuario->id}}" method="POST">
                                    {{ method_field('PUT') }}
                                    {{ csrf_field() }}
                                    <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
                                        <input class="form-control has-feedback-left" id="name" name="name" placeholder="Nombre" value="{{$usuario->name}}" type="text">
                                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                        <input class="form-control has-feedback-left" id="email" placeholder="Email" type="email" value="{{$usuario->email}}" name="email">
                                        <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                        <input class="form-control" id="email-verification" placeholder="Verifique el email" type="email" value="{{$usuario->email}}" name="emailVerification">
                                        <span class="fa fa-envelope form-control-feedback right" aria-hidden="true"></span>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Carrera</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <select id="carrera" class="form-control" name="carrera">
                                                @foreach( $carreras as $carrera)
                                                    @if($usuario->carrera_id == $carrera->id)
                                                        <option value="{{ $carrera->id }}" selected> {{$carrera->nombre}} </option>
                                                    @else
                                                        <option value="{{ $carrera->id }}"> {{$carrera->nombre}} </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!-- FALTA IMPLEMENTAR

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Contraseña</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input  class="form-control" id="password" name="password" type="password">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Verificar contraseña</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input  class="form-control" id="password" name="passwordVerification" type="password">
                                        </div>
                                    </div>
                                    -->

                                    <div class="form-group">
                                        <label class="col-md-3 col-sm-3 col-xs-12 control-label">¿Es admin?
                                        </label>

                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <div class="checkbox">
                                                <label>
                                                    <input value="" type="checkbox" name="is-admin">
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                            <a class="btn btn-primary" href="/usuarios/{{$usuario->id}}"> Cancelar</a>
                                            <button class="btn btn-primary" type="reset">Reset</button>
                                            <button type="submit" class="btn btn-success">Aceptar</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--        @include('template.components.content')--}}
    </div>
</div>

@endsection
@section('scripts')
<script>
    var name=document.getElementById("name");
    var email=document.getElementById("email");
    var email2=document.getElementById("email-verification");
    var carrrera=document.getElementById("carrera");

    console.log(name.value);

</script>

@endsection