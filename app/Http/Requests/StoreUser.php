<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * Validacion necesaria para la creacion y actualización de usuario
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'required|email|same:emailVerification', //Falta añadir que sea unico, pero eso complica el update
            'emailVerification'=>'required',
            'password'=>'sometimes|required|same:passwordVerification',
            'passwordVerification'=>'sometimes|required',
            'name'=>'required|string',
            'carrera'=>'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El nombre del usuario es requerido.',
            'email.required' => 'El email es requerido.',
            'emailVerification.required'  => 'La verificación del email es requerida.',
            'email.email' => 'El email ingresado debe ser valido.',
            'email.same' => 'Los emails deben coincidir.',
            'password.required'=>'La contraseña es requerida.',
            'password.same'=>'Las contraseñas deben coincidir.',
            'passwordVerification.required'=>'La verificación de contraseña es requerida.',
            'carrera.required' => 'La carrera es requerida.'
        ];
    }
}
