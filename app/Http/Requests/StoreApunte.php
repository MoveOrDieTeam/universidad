<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreApunte extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tit' => 'bail|required|max:35|string',
            'desc' => 'nullable|max:250',
            'mat' => 'required',
            'anioElegido' => 'required',
            'usuarioElegido' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'tit.required' => 'El título es requerido obligatoriamente',
            'tit.max' => 'El título no puede contener más de 35 caracteres',
            'desc.max' => 'La descripción no puede contener más de 250 caracteres'
        ];
    }

}
