<?php

namespace App\Http\Controllers;

use App\Apunte;
use App\Carrera;
use App\Http\Requests\StoreApunte;
use App\Instituto;
use App\Materia;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ApuntesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $apuntes = DB::table('apuntes')
            ->join('materias','apuntes.materia_id','=','materias.id')
            ->select('apuntes.*','materias.nombre as materia_nombre')
            ->get();

        return view('apuntes/apuntes',['apuntes' => $apuntes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $listaMaterias = Materia::all();
        $listaUsuarios = User::all();
        return view('apuntes/apuntesCreate',['listaMaterias' => $listaMaterias, 'listaUsuarios' => $listaUsuarios]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreApunte $request)
    {
        $apunteNuevo = new Apunte;
        $apunteNuevo->titulo = $request->tit;
        $apunteNuevo->descripcion = $request->desc;
        $apunteNuevo->materia_id = $request->mat;
        $apunteNuevo->anio = $request->anioElegido;
        $apunteNuevo->cuatrimestre = $request->radioCuatri;
        $apunteNuevo->usuario_id = $request->usuarioElegido;
        $auxDirectorio = "\home\apuntes\ " . $request->tit;  // Si pones: \" .Te cancela el string #Consulta
        $apunteNuevo->path = $auxDirectorio;
        $apunteNuevo->fecha = date("Y-d-m");
        $apunteNuevo->es_privado = $request->radioPrivado;
        $apunteNuevo->save();

        // Se crea con la ID última por más de que hayas borrado apuntes. #REVISAR

        $apuntes = DB::table('apuntes') //Hay que cambiarlo por el otro método, uno más "directo
            ->join('materias','apuntes.materia_id','=','materias.id')
            ->select('apuntes.*','materias.nombre as materia_nombre')
            ->get();

        \Session::flash('success_message_store','Su apunte fue creado exitosamente.');
        return view('apuntes/apuntes',['apuntes' => $apuntes]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $apunte = Apunte::find($id);
        $usuario = User::find($apunte->usuario_id);
        $materia = Materia::find($apunte->materia_id);
        return view('apuntes/apuntesShow',['apunte' => $apunte, 'materia' => $materia, 'usuario' =>$usuario]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $apunte = Apunte::find($id);
        $usuario = User::find($apunte->usuario_id);
        $materia = Materia::find($apunte->materia_id);
        $listaMaterias = Materia::all();
        return view('apuntes/apuntesEdit',['apunte' => $apunte, 'materia' => $materia, 'usuario' =>$usuario,'listaMaterias' => $listaMaterias]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tit = $request-> tit;
        $desc = $request-> desc;
        $mat = $request-> mat;
        $cuatri = $request->radioCuatri;
        $año = $request->anioElegido;
        $priv = $request->radioPrivado;

        $apunteEdit = Apunte::find($id);
        if($tit != null) $apunteEdit->titulo = $tit;
        if($desc != null) $apunteEdit->descripcion = $desc;
        if($mat != null) $apunteEdit->materia_id = $mat;
        if($cuatri != null) $apunteEdit->cuatrimestre = $cuatri;
        if($año != null) $apunteEdit->anio = $año;
        if($priv != null) $apunteEdit->es_privado = $priv;

        $apunteEdit->save();

        $apuntes = DB::table('apuntes') //Hay que cambiarlo por el otro método, uno más "directo.
        ->join('materias','apuntes.materia_id','=','materias.id')
            ->select('apuntes.*','materias.nombre as materia_nombre')
            ->get();

        \Session::flash('success_message_update','Su apunte fue editado exitosamente.');
        return view('apuntes/apuntes',['apuntes' => $apuntes]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (Apunte::find($id) == null)
        {
            \Session::flash('failure_message_destroy','El apunte que intentaba eliminar no existe o ya fue eliminado anteriormente.');
        }
        else
        {
            $apunteDelete = Apunte::find($id);
            $apunteDelete->delete();
            \Session::flash('success_message_destroy','Su apunte fue eliminado exitosamente.');
        }

        $apuntes = DB::table('apuntes') //Hay que cambiarlo por el otro método, uno más "directo.
        // Quizás se lo puede cambiar por una variable global de este controlador, siempre estoy usando el mismo. #Consulta
        ->join('materias','apuntes.materia_id','=','materias.id')
            ->select('apuntes.*','materias.nombre as materia_nombre')
            ->get();

        return view('apuntes/apuntes',['apuntes' => $apuntes]);

    }


}
