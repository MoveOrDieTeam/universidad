<?php

namespace App\Http\Controllers;

use App\Apunte;
use App\Carrera;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\StoreUser;
use Illuminate\Support\Facades\DB;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios =  DB::table('users')
            ->join('carreras','users.carrera_id','=','carreras.id')
            ->select('users.*','carreras.nombre as carrera_nombre')
            ->get();
        return view('users/usuarios',['usuarios' =>$usuarios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $carreras = Carrera::all();
        return view('users/usuariosCreate',['carreras' => $carreras]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser $request)
    {
        $request->validated();
        $usuario= new User;
        $usuario->password=$request->password;
        $this->saveUser($request, $usuario);


        return $this->index();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = User::find($id);
        $carrera = $usuario->carrera()->first();
        $cantApuntes = $usuario->apuntes()->count();
        return view('users/usuariosShow',['usuario'=>$usuario, 'carrera'=>$carrera ,'apuntes'=>$cantApuntes]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /*Aun no cambia la contraseña*/
        $usuario= User::find($id);
        $carreras= Carrera::all();

        return view('users/usuariosEdit',['usuario'=>$usuario, 'carreras'=>$carreras]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUser $request, $id)
    {
        $request->validated();
        $usuario= User::find($id);
        $this->saveUser($request, $usuario);
        return $this->show($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::find($id);
        Apunte::where('usuario_id',$id)->delete();
        $user->delete();
        return $this->index();
    }

    /**/
    public function saveUser(Request $request,User $usuario){

        $usuario->name=$request->name;
        $usuario->email=$request->email;
        $usuario->carrera_id= $request->carrera;
        $usuario->save();
    }
}
