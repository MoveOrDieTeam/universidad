<?php

namespace App\Http\Controllers;

use App\Carrera;
use App\Http\Requests\StoreUser;
use App\User;
use Illuminate\Http\Request;

class PerfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = User::find($id);
        $carrera = $usuario->carrera()->first();
        $cantApuntes = $usuario->apuntes()->count();
        $apuntes = $usuario->apuntes()->get();
        return view('perfil/perfilShow',['usuario'=>$usuario, 'carrera'=>$carrera ,'cantApuntes'=>$cantApuntes, 'apuntes'=>$apuntes]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario= User::find($id);

        $carrera = $usuario->carrera()->first();
        $cantApuntes = $usuario->apuntes()->count();
        $carreras= Carrera::all();

        return view('perfil/editProfile',['usuario'=>$usuario, 'carrera'=>$carrera ,'cantApuntes'=>$cantApuntes, 'carreras'=>$carreras]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUser $request, $id)
    {
        $request->validated();
        $usuario=User::find($id);

        $usuario->name=$request->name;
        $usuario->email=$request->email;
        $usuario->carrera_id= $request->carrera;
        $usuario->save();


        return $this->show($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
