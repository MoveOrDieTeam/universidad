<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;


class Apunte extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'apuntes';

    protected $fillable = ['anio','cuatrimestre', 'es_privado','usuario_id'];


    public function user()
    {
        return $this -> belongsTo(User::class);
    }

    public function materia()
    {
        return $this->belongsTo(Materia::class);
    }

}
