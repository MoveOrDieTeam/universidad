<?php

namespace App;

use App\Instituto;
use App\Materia;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Carrera extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */

    protected $table = 'carreras';
    protected $fillable=[
        'nombre',
        'instituto_id'
    ];


    public function instituto()
    {
        return $this->belongsTo(Instituto::class);

    }

    public function materias()
    {
        return $this->belongsToMany('App\Materia','carrera_materia','carrera_id','materia_id');
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

}
