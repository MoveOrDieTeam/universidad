<?php

namespace App\Console\Commands;

use App\Apunte;
use App\Carrera;
use App\Instituto;
use App\Materia;
use App\User;
use Illuminate\Console\Command;

class test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Test Carrera->materias\n";
        print_r(Carrera::with('materias')->get()->toArray());
        echo "Test Carrera->instituto\n";
        print_r(Carrera::with('instituto')->get()->toArray());
        echo "Test Carrera->users\n";
        print_r(Carrera::with('users')->get()->toArray());
        echo "Test Materia->carreras\n";
        print_r(Materia::with('carreras')->get()->toArray());
        echo "Test Materia->apuntes\n";
        print_r(Materia::with('apuntes')->get()->toArray());
        echo "Test Apunte->materias\n";
        print_r(Apunte::with('materia')->get()->toArray());
        echo "Test Apunte->user\n";
        print_r(Apunte::with('user')->get()->toArray());
        echo "Test Instituto->carreras\n";
        print_r(Instituto::with('carreras')->get()->toArray());
        echo "Test User->apuntes\n";
        print_r(User::with('apuntes')->get()->toArray());
        echo "Test User->apuntes\n";
        print_r(User::with('carrera')->get()->toArray());
    }
}
