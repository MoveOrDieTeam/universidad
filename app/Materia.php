<?php

namespace App;


use App\Carrera;
use Illuminate\Database\Eloquent\Model;

class Materia extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */

    protected $table = 'materias';
    protected $fillable=[
        'nombre',
        'codigo'
    ];

    public function carreras()
    {
        return $this->belongsToMany('App\Carrera','carrera_materia','materia_id','carrera_id');

    }

    public function apuntes()
    {
        return $this->hasMany(Apunte::class);
    }

}
