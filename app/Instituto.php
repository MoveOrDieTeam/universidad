<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instituto extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */

    protected $table = 'institutos';
    protected $fillable=['nombre'];

    public function carreras()
    {
        return $this->hasMany(Carrera::class);
    }

}
